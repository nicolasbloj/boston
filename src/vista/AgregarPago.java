/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

//import controlador.manejadorCondVta;
//import controlador.manejadorCtaCte;
//import controlador.manejadorFacturacion;
import controlador.manejadorCondVta;
import controlador.manejadorConfiguraciones;
import controlador.manejadorControl;
import controlador.manejadorCostos;
import controlador.manejadorFacturacion;
import controlador.manejadorFechas;
import controlador.manejadorImpresion;
import controlador.manejadorReportes;
import controlador.manejadorTablas;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.Boleta;
import modelo.Cliente;
import modelo.Cond_vta;
import modelo.Costo;
import modelo.Detalle_boleta;
import modelo.Facturacion_boleta;
import modelo.Facturacion_costo;
import modelo.Usuario;
import org.jfree.data.category.DefaultCategoryDataset;
import vista.Gestion_Costos.ModeloTablaCostos;
import vista.Inicio.ModeloTablaInicio;
import vista.Inicio.ModeloTablaMesasOcupadas;

/**
 *
 * @author rmmayer
 */
public class AgregarPago extends javax.swing.JFrame {

    /**
     * Creates new form AgregarPagoCtaCte
     */
    manejadorCondVta mcv = new manejadorCondVta();
    manejadorFacturacion mf = new manejadorFacturacion();
    manejadorTablas mt = new manejadorTablas();
    manejadorCostos mc = new manejadorCostos();
    manejadorImpresion mi = new manejadorImpresion();
    manejadorControl mcon = new manejadorControl();
    manejadorReportes mr = new manejadorReportes();
    manejadorConfiguraciones mconf = new manejadorConfiguraciones();
    manejadorFechas mfechas = new manejadorFechas();

    Cond_vta cv = new Cond_vta();
    Usuario usuario = new Usuario();
    Cliente cliente = new Cliente();
    Boleta boleta = new Boleta();
    Costo cost = new Costo();
    Detalle_boleta detbol = new Detalle_boleta();
    Facturacion_boleta fb = new Facturacion_boleta();
    Facturacion_costo fc = new Facturacion_costo();
    JTable tc = new JTable();
    JFrame gcc = new JFrame();
    Inicio inicio;
    int id_cc;
    int id_tipo_pago;
    int t;
    float imp;
    boolean b = true;
    static ArrayList<Detalle_boleta> aD = new ArrayList<Detalle_boleta>();
    DefaultCategoryDataset dB;
    DefaultTableModel dtm;
    DefaultTableModel dmm;
    DefaultTableModel dmc;

    public class ModeloTablaFacturacionCuotas extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    ModeloTablaFacturacionCuotas modeloTablaFacturacionCuotas = new ModeloTablaFacturacionCuotas();

    public AgregarPago(Usuario u, Cliente c, Boleta b, Costo costo, Inicio ini, ArrayList<Detalle_boleta> arrayDetalles, DefaultCategoryDataset datosBarriles, ModeloTablaInicio mti, ModeloTablaMesasOcupadas mmo, ModeloTablaCostos mtc, int tipo) {

        initComponents();
        this.setFocusable(true);
        this.requestFocus();
        usuario = u;
        boleta = b;
        cliente = c;
        inicio = ini;
        cost = costo;
        //imp = impresion;
        this.botonEliminarPago.setEnabled(false);
        aD = arrayDetalles;
        dB = datosBarriles;
        dtm = mti;
        dmm = mmo;
        dmc = mtc;
        txtMonto.requestFocus();
        t = tipo;

        mt.diseñoTablaFacturacionCuotas(tablaPagos, modeloTablaFacturacionCuotas);
        mcv.cargarCondVtaSinCuota(JComboBoxDetallePago);

        if (t == 3) { // tipo 3 es porque viene de Gestion de Costos
            this.txtTotal.setText(String.valueOf(costo.getValor()));
            this.txtTotalRestante.setText(String.valueOf(costo.getValor()));
            this.setTitle("PAGO DE COSTO");
        } else {

            String user = usuario.getNombre_usuario() + " " + usuario.getApellido_usuario();
            this.setTitle("( User Conectado: " + user + " )");

            this.txtTotal.setText(String.valueOf(boleta.getTotal()));
            this.txtTotalRestante.setText(String.valueOf(boleta.getTotal()));

            for (int i = 0; i < aD.size(); i++) {
                System.out.println(aD.get(i).getCodigo() + " " + aD.get(i).getCantidad() + " " + aD.get(i).getPrecio());
            }
        }

        this.botonConfirmarPago.setEnabled(false);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtMonto = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        JComboBoxDetallePago = new javax.swing.JComboBox<>();
        botonAñadir = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        txtTotal = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtTotalRestante = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaPagos = new javax.swing.JTable();
        botonConfirmarPago = new javax.swing.JButton();
        botonEliminarPago = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("INSERTAR FORMA DE PAGO"));

        jLabel1.setText("Monto:");

        txtMonto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtMontoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtMontoKeyReleased(evt);
            }
        });

        jLabel2.setText("Detalle: ");

        JComboBoxDetallePago.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        JComboBoxDetallePago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JComboBoxDetallePagoActionPerformed(evt);
            }
        });
        JComboBoxDetallePago.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                JComboBoxDetallePagoPropertyChange(evt);
            }
        });

        botonAñadir.setText("AÑADIR");
        botonAñadir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAñadirActionPerformed(evt);
            }
        });

        jLabel6.setText("TOTAL A PAGAR:");

        txtTotal.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N

        jLabel7.setText("TOTAL RESTANTE:");

        txtTotalRestante.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(botonAñadir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel1)
                                .addComponent(jLabel2))
                            .addGap(35, 35, 35)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtMonto)
                                .addComponent(JComboBoxDetallePago, 0, 172, Short.MAX_VALUE)))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel7)
                                .addComponent(jLabel6))
                            .addGap(18, 18, 18)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtTotalRestante, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(0, 26, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtMonto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(JComboBoxDetallePago, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addComponent(botonAñadir, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtTotalRestante, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        tablaPagos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaPagos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaPagosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaPagos);

        botonConfirmarPago.setText("CONFIRMAR PAGO");
        botonConfirmarPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonConfirmarPagoActionPerformed(evt);
            }
        });
        botonConfirmarPago.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                botonConfirmarPagoKeyPressed(evt);
            }
        });

        botonEliminarPago.setText("ELIMINAR PAGO");
        botonEliminarPago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarPagoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonEliminarPago, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonConfirmarPago, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(33, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botonEliminarPago, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(botonConfirmarPago, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void JComboBoxDetallePagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JComboBoxDetallePagoActionPerformed


    }//GEN-LAST:event_JComboBoxDetallePagoActionPerformed

    private void JComboBoxDetallePagoPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_JComboBoxDetallePagoPropertyChange
        // TODO add your handling code here:
    }//GEN-LAST:event_JComboBoxDetallePagoPropertyChange

    private void botonAñadirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAñadirActionPerformed
        // TODO add your handling code here:
        if (!botonConfirmarPago.isEnabled()) {
            if (this.txtMonto.getText().isEmpty() || this.txtMonto.getText().equals("")) {
                String formaPago = this.JComboBoxDetallePago.getSelectedItem().toString();
                cv = mcv.obtenerCond_cta(cv, formaPago);
                //txtTotalRestante.setText(Float.parseFloat(formaPago)txtTotalRestante);
                Object[] art = new Object[3];
                art[0] = cv.getId_condvta();
                art[1] = cv.getDesc_condvta();
                art[2] = txtTotalRestante.getText();

                this.modeloTablaFacturacionCuotas.addRow(art);
                txtTotalRestante.setText("0");
                txtMonto.setText("");

                if (Float.parseFloat(txtTotalRestante.getText()) == 0) {
                    botonConfirmarPago.setEnabled(true);
                    this.botonAñadir.setEnabled(false);
                    this.txtMonto.setEnabled(false);
                    botonConfirmarPago.requestFocus();
                } else {
                    this.txtMonto.setEnabled(true);
                }
            } else if (Float.parseFloat(this.txtMonto.getText()) < 0 || Float.parseFloat(this.txtMonto.getText()) > Float.parseFloat(this.txtTotalRestante.getText()) || !mcon.isNumeric(txtMonto.getText())) {
                JOptionPane.showMessageDialog(null, "INGRESE UN MONTO VÁLIDO");
            } else {
                String formaPago = this.JComboBoxDetallePago.getSelectedItem().toString();
                cv = mcv.obtenerCond_cta(cv, formaPago);
                //txtTotalRestante.setText(Float.parseFloat(formaPago)txtTotalRestante);
                Object[] art = new Object[3];
                art[0] = cv.getId_condvta();
                art[1] = cv.getDesc_condvta();
                art[2] = txtMonto.getText();

                this.modeloTablaFacturacionCuotas.addRow(art);
                txtTotalRestante.setText(String.valueOf(Float.parseFloat(txtTotalRestante.getText()) - Float.parseFloat(txtMonto.getText())));
                txtMonto.setText("");

                if (Float.parseFloat(txtTotalRestante.getText()) == 0) {
                    botonConfirmarPago.setEnabled(true);
                    this.botonAñadir.setEnabled(false);
                    this.txtMonto.setEnabled(false);
                    botonConfirmarPago.requestFocus();
                } else {
                    this.txtMonto.setEnabled(true);
                }
            }
        }


    }//GEN-LAST:event_botonAñadirActionPerformed

    private void botonConfirmarPagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonConfirmarPagoActionPerformed
        // TODO add your handling code here:

        if (t == 1) {
            mf.insertarBoleta(boleta, 1);

            boleta.setId_boleta(mf.obtenerID_boleta());

            for (int i = 0; i < aD.size(); i++) {
                aD.get(i).setId_boleta(boleta.getId_boleta());
                mf.insertarDetallesBoleta(aD.get(i), dB);
            }

        } else if (t == 3) {
            mc.insertarCosto(cost);
            cost.setId_costo(mc.obtenerCostoID());
        }

        for (int i = 0; i < this.tablaPagos.getRowCount(); i++) {
            int id_condPago = Integer.parseInt(this.tablaPagos.getValueAt(i, 0).toString());
            float monto = Float.parseFloat(this.tablaPagos.getValueAt(i, 2).toString());
            if (t == 3) {
                fc.setId_costo(cost.getId_costo());
                fc.setId_condVta(id_condPago);
                fc.setMonto(monto);
                mf.insertarFormasDePagoCosto(fc);
            } else {
                fb.setId_boleta(boleta.getId_boleta());
                fb.setId_condvta(id_condPago);
                fb.setTotal(monto);
                mf.insertarFormasDePago(fb);
            }

        }

        JOptionPane.showMessageDialog(null, "EL PAGO SE REALIZO CON EXITO");

        this.dispose();

        if (t == 3) {
            //mt.traerCostosDelDia((ModeloTablaCostos) dmc);
            Gestion_Costos gc = new Gestion_Costos();
            gc.setLocationRelativeTo(null);
            gc.setVisible(true);

        } else {
            mf.pagarBoleta(boleta.getId_boleta());
            //mt.agregarVentaRapida((ModeloTablaInicio) dtm);
            mt.traerVentasSinCerrar((ModeloTablaInicio) dtm);

            if (t == 2) {
                System.out.println("-----------------ENTRO A ACTUALIZAR LA BOLETA-------------------------");
                mf.setHoraCierreBoleta(mfechas.obtenerHoraActual(), mfechas.obtenerFechaActual(), boleta.getId_boleta());
                boleta.setHora_cierre(mfechas.obtenerHoraActual());
                boleta.setFecha_cierre(mfechas.obtenerFechaActual());
                System.out.println("FECHA Y HORA DE CIERRE: "+mfechas.convertirFecha(boleta.getFecha_cierre())+" "+boleta.getHora_cierre().substring(0, 5));
                imp = mconf.valorVariableString("Impresion mesa").getValor_configuracion();
                mt.traerMesasOcupadas((ModeloTablaMesasOcupadas) dmm);
                if (imp != 0) {
                    System.out.println("se va a imprimir por mesa");
                    mi.imprimirFactura(boleta, cliente, aD);
                }
            } else {
                imp = mconf.valorVariableString("Impresion rapida").getValor_configuracion();
                System.out.println("FECHA Y HORA DE CIERRE: "+mfechas.convertirFecha(boleta.getFecha_cierre())+" "+boleta.getHora_cierre().substring(0, 5));
                if (imp != 0) {
                    System.out.println("se va a imprimir rapida");
                    mi.imprimirFacturaVtaRapida(boleta, cliente, aD);
                }
            }

            mr.ventasActuales(inicio.datosVentas);
        }


    }//GEN-LAST:event_botonConfirmarPagoActionPerformed

    private void botonEliminarPagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarPagoActionPerformed
        // TODO add your handling code here:
        int filaS = this.tablaPagos.getSelectedRow();
        this.txtMonto.setEnabled(true);
        this.botonConfirmarPago.setEnabled(false);
        float suma = Float.parseFloat(this.tablaPagos.getValueAt(filaS, 2).toString());
        float nuevo = Float.parseFloat(txtTotalRestante.getText()) + suma;

        this.txtTotalRestante.setText(String.valueOf(nuevo));

        this.botonAñadir.setEnabled(true);
        txtMonto.requestFocus();
        this.modeloTablaFacturacionCuotas.removeRow(filaS);

        if (this.tablaPagos.getRowCount() == 0) {
            this.botonEliminarPago.setEnabled(false);
        } else {
            this.botonEliminarPago.setEnabled(true);
        }


    }//GEN-LAST:event_botonEliminarPagoActionPerformed

    private void tablaPagosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaPagosMouseClicked
        // TODO add your handling code here:

        if (this.tablaPagos.getSelectedRowCount() != 0) {
            this.botonEliminarPago.setEnabled(true);
        } else {
            this.botonEliminarPago.setEnabled(false);
        }
    }//GEN-LAST:event_tablaPagosMouseClicked

    private void txtMontoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMontoKeyReleased
        // TODO add your handling code here:


    }//GEN-LAST:event_txtMontoKeyReleased

    private void txtMontoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMontoKeyPressed
        // TODO add your handling code here:
        if (!botonConfirmarPago.isEnabled()) {
            if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                if (this.txtMonto.getText().isEmpty() || this.txtMonto.getText().equals("")) {
                    String formaPago = this.JComboBoxDetallePago.getSelectedItem().toString();
                    cv = mcv.obtenerCond_cta(cv, formaPago);
                    //txtTotalRestante.setText(Float.parseFloat(formaPago)txtTotalRestante);
                    Object[] art = new Object[3];
                    art[0] = cv.getId_condvta();
                    art[1] = cv.getDesc_condvta();
                    art[2] = txtTotalRestante.getText();

                    this.modeloTablaFacturacionCuotas.addRow(art);
                    txtTotalRestante.setText("0");
                    txtMonto.setText("");

                    if (Float.parseFloat(txtTotalRestante.getText()) == 0) {
                        botonConfirmarPago.setEnabled(true);
                        this.botonAñadir.setEnabled(false);
                        this.txtMonto.setEnabled(false);
                        botonConfirmarPago.requestFocus();
                    } else {
                        this.txtMonto.setEnabled(true);
                    }
                } else if (Float.parseFloat(this.txtMonto.getText()) < 0 || Float.parseFloat(this.txtMonto.getText()) > Float.parseFloat(this.txtTotalRestante.getText()) || !mcon.isNumeric(txtMonto.getText())) {
                    JOptionPane.showMessageDialog(null, "INGRESE UN MONTO VÁLIDO");
                } else {
                    String formaPago = this.JComboBoxDetallePago.getSelectedItem().toString();
                    cv = mcv.obtenerCond_cta(cv, formaPago);
                    //txtTotalRestante.setText(Float.parseFloat(formaPago)txtTotalRestante);
                    Object[] art = new Object[3];
                    art[0] = cv.getId_condvta();
                    art[1] = cv.getDesc_condvta();
                    art[2] = txtMonto.getText();

                    this.modeloTablaFacturacionCuotas.addRow(art);
                    txtTotalRestante.setText(String.valueOf(Float.parseFloat(txtTotalRestante.getText()) - Float.parseFloat(txtMonto.getText())));
                    txtMonto.setText("");

                    if (Float.parseFloat(txtTotalRestante.getText()) == 0) {
                        botonConfirmarPago.setEnabled(true);
                        this.botonAñadir.setEnabled(false);
                        this.txtMonto.setEnabled(false);
                        botonConfirmarPago.requestFocus();
                    } else {
                        this.txtMonto.setEnabled(true);
                    }
                }

            }
        }
    }//GEN-LAST:event_txtMontoKeyPressed

    private void botonConfirmarPagoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_botonConfirmarPagoKeyPressed
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (t == 1) {
                mf.insertarBoleta(boleta, 1);

                boleta.setId_boleta(mf.obtenerID_boleta());

                for (int i = 0; i < aD.size(); i++) {
                    aD.get(i).setId_boleta(boleta.getId_boleta());
                    mf.insertarDetallesBoleta(aD.get(i), dB);
                }

            } else if (t == 3) {
                mc.insertarCosto(cost);
                cost.setId_costo(mc.obtenerCostoID());
            }

            for (int i = 0; i < this.tablaPagos.getRowCount(); i++) {
                int id_condPago = Integer.parseInt(this.tablaPagos.getValueAt(i, 0).toString());
                float monto = Float.parseFloat(this.tablaPagos.getValueAt(i, 2).toString());
                if (t == 3) {
                    fc.setId_costo(cost.getId_costo());
                    fc.setId_condVta(id_condPago);
                    fc.setMonto(monto);
                    mf.insertarFormasDePagoCosto(fc);
                } else {
                    fb.setId_boleta(boleta.getId_boleta());
                    fb.setId_condvta(id_condPago);
                    fb.setTotal(monto);
                    mf.insertarFormasDePago(fb);
                }

            }

            JOptionPane.showMessageDialog(null, "EL PAGO SE REALIZO CON EXITO");

            this.dispose();

            if (t == 3) {
                //mt.traerCostosDelDia((ModeloTablaCostos) dmc);
                Gestion_Costos gc = new Gestion_Costos();
                gc.setLocationRelativeTo(null);
                gc.setVisible(true);

            } else {
                mf.pagarBoleta(boleta.getId_boleta());
                //mt.agregarVentaRapida((ModeloTablaInicio) dtm);
                mt.traerVentasSinCerrar((ModeloTablaInicio) dtm);
                System.out.println("impresion mesa: " + mconf.valorVariableString("Impresion Mesa").getValor_configuracion());
                System.out.println("impresion rapida: " + mconf.valorVariableString("Impresion Rapida").getValor_configuracion());

                if (t == 2) {
                    System.out.println("-----------------ENTRO A ACTUALIZAR LA BOLETA-------------------------");
                    mf.setHoraCierreBoleta(mfechas.obtenerHoraActual(), mfechas.obtenerFechaActual(), boleta.getId_boleta());
                    boleta.setHora_cierre(mfechas.obtenerHoraActual());
                    boleta.setFecha_cierre(mfechas.obtenerFechaActual());
                    imp = mconf.valorVariableString("Impresion Mesa").getValor_configuracion();
                    mt.traerMesasOcupadas((ModeloTablaMesasOcupadas) dmm);
                    System.out.println("FECHA Y HORA DE CIERRE: "+mfechas.convertirFecha(boleta.getFecha_cierre())+" "+boleta.getHora_cierre().substring(0, 5));
                    if (imp != 0) {
                        mi.imprimirFactura(boleta, cliente, aD);
                        System.out.println("VA A IMPRIMIR LA MESA");
                    }
                } else {
                    imp = mconf.valorVariableString("Impresion Rapida").getValor_configuracion();
                    System.out.println("FECHA Y HORA DE CIERRE: "+mfechas.convertirFecha(boleta.getFecha_cierre())+" "+boleta.getHora_cierre().substring(0, 5));
                    if (imp != 0) {
                        mi.imprimirFacturaVtaRapida(boleta, cliente, aD);
                        System.out.println("VA A IMPRIMIR LA VENTA RAPIDA");
                    }
                }

                mr.ventasActuales(inicio.datosVentas);
            }
        }
    }//GEN-LAST:event_botonConfirmarPagoKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AgregarPago.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AgregarPago.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AgregarPago.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AgregarPago.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AgregarPago(null, null, null, null, null, null, null, null, null, null, 0).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> JComboBoxDetallePago;
    private javax.swing.JButton botonAñadir;
    private javax.swing.JButton botonConfirmarPago;
    private javax.swing.JButton botonEliminarPago;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaPagos;
    private javax.swing.JTextField txtMonto;
    private javax.swing.JLabel txtTotal;
    private javax.swing.JLabel txtTotalRestante;
    // End of variables declaration//GEN-END:variables
}
