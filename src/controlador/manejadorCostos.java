/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import boston.conexionDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import modelo.Costo;
import modelo.Tipo_costo;

/**
 *
 * @author rmmayer
 */
public class manejadorCostos {

    public ArrayList<Tipo_costo> traerTiposCosto(JComboBox j) {
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        ArrayList<Tipo_costo> a = new ArrayList<>();

        j.removeAllItems();

        String sql = "SELECT id_tipoCosto, desc_tipoCosto "
                + "FROM tipo_costo";

        System.out.println(sql);

        Tipo_costo tc = new Tipo_costo();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                Tipo_costo tipo = new Tipo_costo();
                tipo.setId_tipoCosto(rs1.getInt(1));
                tipo.setDesc_tipoCosto(rs1.getString(2));
                a.add(tipo);
                tc.setId_tipoCosto(tipo.getId_tipoCosto());
                tc.setDesc_tipoCosto(tipo.getDesc_tipoCosto());
                j.addItem(tc.getDesc_tipoCosto());

            }
            j.setSelectedIndex(-1);
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

        return a;

    }

    public int insertarNuevoTipoCosto(String descripcion) {
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        int n = 0;

        String sql = "INSERT into TIPO_COSTO (desc_tipocosto) VALUES (?)";
        System.out.println(sql);

        try {

            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, descripcion);

            n = ps.executeUpdate();
            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

        return n;
    }

    public int insertarCosto(Costo costo) {
        int n = 0;
        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        String sql = "INSERT into COSTO (id_tipoCosto, desc_costo, valor, fecha_costo, cerrado, hora_costo) VALUES (?,?,?,?,?,?)";
        System.out.println(sql);

        try {

            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, costo.getId_tipoCosto());
            ps.setString(2, costo.getDesc_costo());
            ps.setFloat(3, costo.getValor());
            ps.setString(4, costo.getFecha_costo());
            ps.setInt(5, 0);
            ps.setString(6, costo.getHora_costo());

            n = ps.executeUpdate();
            con.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

        return n;

    }

    public int obtenerCostoID() {
        int ultCosto = 0;

        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        String sql = "SELECT id_costo from costo order by id_costo desc limit 1";
        System.out.println(sql);

        try {
            Statement st = reg.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                ultCosto = rs.getInt(1);
                System.out.println("ULTIMO COSTO: " + ultCosto);
            }

            reg.close();

        } catch (SQLException e) {
            System.out.println("ERROR AL TRAER ULTIMA BOLETA");
        }

        return ultCosto;
    }

    public int actualizarCosto(Tipo_costo tc) {
        int n = 0;
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();

        String sql = "UPDATE Tipo_costo "
                + "SET desc_tipoCosto = '" + tc.getDesc_tipoCosto() + "' "
                + "WHERE id_tipoCosto = " + tc.getId_tipoCosto();

        Statement st1;
        try {
            st1 = reg.createStatement();
            n = st1.executeUpdate(sql);

            reg.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorArticulos.class.getName()).log(Level.SEVERE, null, ex);
        }

        return n;

    }
    
    public void eliminarCosto (int id){
        conexionDB con = new conexionDB();
        Connection reg = con.conectar();
        
        String sql = "DELETE FROM Costo "
                + "WHERE id_costo = "+id;
        
        Statement st1;
        try {
            st1 = reg.createStatement();
            st1.executeUpdate(sql);

            reg.close();

        } catch (SQLException ex) {
            Logger.getLogger("ERROR AL ELIMINAR COSTO: "+ex);
        }
    }

}
