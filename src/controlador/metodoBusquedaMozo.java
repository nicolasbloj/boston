/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import boston.conexionDB;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.DefaultComboBoxModel;
import vista.NuevaMesa;

/**
 *
 * @author rmmayer
 */
public class metodoBusquedaMozo {

    public NuevaMesa c;
    //public JFrame cpd;

    public metodoBusquedaMozo(NuevaMesa c) {
        this.c = c;
    }

    

    public boolean comparar(String cadena) {
        Object[] lista = this.c.getcomboBoxMozo().getSelectedObjects();//       traer todos los items mejor.
        boolean encontrado = false;

        int xx = this.c.getcomboBoxMozo().getItemCount();

        for (int i = 0; i < xx; i++) {
            //  System.out.println("elemento seleccionado - "+lista[i]);
            if (cadena.equals(this.c.getcomboBoxMozo().getItemAt(i))) {
                //  nn=(String)boxNombre.getItemAt(i).toString(); 
                encontrado = true;
                break;
            }

        }
        return encontrado;

    }

    public DefaultComboBoxModel getLista(String cadenaEscrita) {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();

        try {
            conexionDB con = new conexionDB();
            Connection conex = con.conectar();
            Statement st1 = conex.createStatement();
            String sql = "select nombre_user, apellido_user from USER WHERE nombre_user LIKE '%" + cadenaEscrita + "%' or apellido_user LIKE '%" + cadenaEscrita + "%'";
            //String sql2 = "select desc_articulo from articulo where desc_articulo LIKE '%"+cadenaEscrita+"%'";
            System.out.println(sql);
            ResultSet res = st1.executeQuery(sql);

            while (res.next()) {
                modelo.addElement(res.getString(1) + " " + res.getString(2));
            }
            res.close();
            conex.close();
        } catch (SQLException ex) {

        }
        return modelo;
    }

}
