/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import boston.conexionDB;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author Rodri Mayer
 */
public class manejadorReportes {

    public void ventasActuales(DefaultPieDataset datosVentas) {
        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        //datosVentas.
        String sql = "SELECT t1.id_tipoProd1, t1.desc_tipoProd1, SUM( d.cantidad * d.precio ) "
                + "FROM Detalle_mesa AS d "
                + "JOIN articulo AS a ON a.codigo = d.codigo "
                + "JOIN Tipo_Prod AS t1 ON a.id_tipoProd = t1.id_tipoProd1 "
                + "JOIN Boleta AS b ON b.id_boleta = d.id_boleta "
                + "WHERE b.cerrada = 0 "
                + "AND b.pagada = 1 "
                + "GROUP BY t1.id_tipoProd1, t1.desc_tipoProd1";

        System.out.println(sql);

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {

                datosVentas.setValue(rs1.getString(2), rs1.getFloat(3));

            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO REPORTE: " + e);
        }

    }

    public void traerVentasPorCierre(DefaultPieDataset datosVentas, int id) {
        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        //datosVentas.
        String sql = "SELECT t1.id_tipoProd1, t1.desc_tipoProd1, SUM( d.cantidad * d.precio ) "
                + "FROM Detalle_mesa AS d "
                + "JOIN articulo AS a ON a.codigo = d.codigo "
                + "JOIN Tipo_Prod AS t1 ON a.id_tipoProd = t1.id_tipoProd1 "
                + "JOIN Boleta AS b ON b.id_boleta = d.id_boleta "
                + "WHERE b.cerrada = " + id + " "
                + "GROUP BY t1.id_tipoProd1, t1.desc_tipoProd1";

        System.out.println(sql);

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {

                datosVentas.setValue(rs1.getString(2), rs1.getFloat(3));

            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO REPORTE: " + e);
        }

    }
    
    public void traerVentasPorCierreLimites(DefaultPieDataset datosVentas, int id_desde, int id_hasta) {
        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        //datosVentas.
        String sql = "SELECT t1.id_tipoProd1, t1.desc_tipoProd1, SUM( d.cantidad * d.precio ) "
                + "FROM Detalle_mesa AS d "
                + "JOIN articulo AS a ON a.codigo = d.codigo "
                + "JOIN Tipo_Prod AS t1 ON a.id_tipoProd = t1.id_tipoProd1 "
                + "JOIN Boleta AS b ON b.id_boleta = d.id_boleta "
                + "WHERE b.cerrada >= " + id_desde + " and b.cerrada < "+id_hasta+" "
                + "GROUP BY t1.id_tipoProd1, t1.desc_tipoProd1";

        System.out.println(sql);

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {

                datosVentas.setValue(rs1.getString(2), rs1.getFloat(3));

            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO REPORTE: " + e);
        }

    }
    
    public void traerCostosPorCierre(DefaultPieDataset datosCostos, int id) {
        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        String sql = "SELECT t.id_tipoCosto, t.desc_tipoCosto, SUM( c.valor ) "
                + "FROM Costo AS c "
                + "JOIN Tipo_Costo AS t ON t.id_tipoCosto = c.id_tipoCosto "
                + "WHERE cerrado = "+id+" "
                + "GROUP BY t.id_tipoCosto, t.desc_tipoCosto";

        System.out.println(sql);

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {

                datosCostos.setValue(rs1.getString(2), rs1.getFloat(3));

            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO REPORTE COSTOS: " + e);
        }

    }
    
    public void traerCostosPorCierreLimites(DefaultPieDataset datosCostos, int id_desde, int id_hasta) {
        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        String sql = "SELECT t.id_tipoCosto, t.desc_tipoCosto, SUM( c.valor ) "
                + "FROM Costo AS c "
                + "JOIN Tipo_Costo AS t ON t.id_tipoCosto = c.id_tipoCosto "
                + "WHERE cerrado >= "+id_desde+" and cerrado < "+id_hasta+" "
                + "GROUP BY t.id_tipoCosto, t.desc_tipoCosto";

        System.out.println(sql);

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {

                datosCostos.setValue(rs1.getString(2), rs1.getFloat(3));

            }

            con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO REPORTE COSTOS: " + e);
        }

    }
    

}
