/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Rodri Mayer
 */
public class Tipo_Prod1 {
    
    private int id_tipoProd1;
    private String desc_tipoProd1;

    public int getId_tipoProd1() {
        return id_tipoProd1;
    }

    public void setId_tipoProd1(int id_tipo) {
        this.id_tipoProd1 = id_tipo;
    }

    public String getDesc_tipoProd1() {
        return desc_tipoProd1;
    }

    public void setDesc_tipoProd1(String desc_tipo) {
        this.desc_tipoProd1 = desc_tipo;
    }
    
    
    
}
