/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author rmmayer
 */
public class Tipo_Prod2 {
    
    private int id_tipoProd2;
    private String desc_tipoProd2;
    private int id_tipoProd1;

    public int getId_tipoProd2() {
        return id_tipoProd2;
    }

    public void setId_tipoProd2(int id_tipoProd2) {
        this.id_tipoProd2 = id_tipoProd2;
    }

    public String getDesc_tipoProd2() {
        return desc_tipoProd2;
    }

    public void setDesc_tipoProd2(String desc_tipoProd2) {
        this.desc_tipoProd2 = desc_tipoProd2;
    }

    public int getId_tipoProd1() {
        return id_tipoProd1;
    }

    public void setId_tipoProd1(int id_tipoProd1) {
        this.id_tipoProd1 = id_tipoProd1;
    }
    
    
    
}
