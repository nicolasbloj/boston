/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author rmmayer
 */
public class Tipo_costo {
    
    private int id_tipoCosto;
    private String desc_tipoCosto;

    public int getId_tipoCosto() {
        return id_tipoCosto;
    }

    public void setId_tipoCosto(int id_tipoCosto) {
        this.id_tipoCosto = id_tipoCosto;
    }

    public String getDesc_tipoCosto() {
        return desc_tipoCosto;
    }

    public void setDesc_tipoCosto(String desc_tipoCosto) {
        this.desc_tipoCosto = desc_tipoCosto;
    }
    
    
    
}
