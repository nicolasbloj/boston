/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author rmmayer
 */
public class Barril {

    private int id_barril;
    private String desc_barril;
    private float cantidad_total;
    private float cantidad_consumida;
    private int posicion;

    public int getId_barril() {
        return id_barril;
    }

    public void setId_barril(int id_barril) {
        this.id_barril = id_barril;
    }

    public String getDesc_barril() {
        return desc_barril;
    }

    public void setDesc_barril(String desc_barril) {
        this.desc_barril = desc_barril;
    }

    public float getCantidad_total() {
        return cantidad_total;
    }

    public void setCantidad_total(float cantidad_total) {
        this.cantidad_total = cantidad_total;

    }

    public float getCantidad_consumida() {
        return cantidad_consumida;
    }

    public void setCantidad_consumida(float cantidad_consumida) {
        this.cantidad_consumida = cantidad_consumida;
    }

    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }

}
